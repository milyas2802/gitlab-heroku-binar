# Docker multi-stage build

# 1. Building the App with Maven
FROM maven:3.8.6-amazoncorretto-17

ADD . /binardemoherokuinfosys
WORKDIR /binardemoherokuinfosys

# Just echo so we can see, if everything is there :)
RUN ls -l

# Run Maven build
RUN mvn clean install


# 2. Just using the build artifact and then removing the build-container
FROM openjdk:17-jdk

MAINTAINER Muhammad Ilyas

VOLUME /tmp

# Add Spring Boot app.jar to Container
COPY --from=0 "/binardemoherokuinfosys/target/binar-demo-heroku-*-SNAPSHOT.jar" app.jar

# Fire up our Spring Boot app by default
#CMD [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]

# Fire up our Spring Boot app by default
CMD [ "sh", "-c", "java $JAVA_OPTS -XX:+UseContainerSupport -Djava.security.egd=file:/dev/./urandom -Dserver.port=$PORT -jar /app.jar" ]
